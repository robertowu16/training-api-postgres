package com.roberto.services;

import java.util.*;
import com.roberto.model.*;

public interface RestService {
	List<RestResult> getAll();

	RestResult getRestById(int id);

	void addRest(Rest rest);

	void updateRest(Rest rest, int id);

	void deleteRestById(int id);

	int lastestInput();
}
