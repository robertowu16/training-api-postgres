package com.roberto.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;
import com.roberto.dao.*;
import com.roberto.model.Rest;
import com.roberto.model.RestResult;

@Service
public class RestServiceImp implements RestService {

	@Autowired
	private RestDao restDao;

	@Override
	public List<RestResult> getAll() {
		return restDao.getAll();
	}

	@Override
	public RestResult getRestById(int id) {
		return restDao.getRestById(id);
	}

	@Override
	public void addRest(Rest rest) {
		restDao.addRest(rest);
	}

	@Override
	public void updateRest(Rest rest, int id) {
		restDao.update(rest, id);
	}

	@Override
	public void deleteRestById(int id) {
		restDao.deleteRestById(id);
	}

	@Override
	public int lastestInput() {
		return restDao.lastestInput();
	}
}
