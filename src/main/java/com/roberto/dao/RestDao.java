package com.roberto.dao;

import com.roberto.model.*;
import java.util.*;

public interface RestDao {

	List<RestResult> getAll();

	RestResult getRestById(int id);

	void addRest(Rest rest);

	void update(Rest rest, int id);

	void deleteRestById(int id);

	int lastestInput();
}
