package com.roberto.server.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.roberto" })
public class PostgresApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PostgresApiApplication.class, args);
	}

}
